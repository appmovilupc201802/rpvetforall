//
//  PetsTableViewController.swift
//  VetForAll
//
//  Created by Alumnos on 10/14/18.
//  Copyright © 2018 UPC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PetsTableViewController: UITableViewController {

    var myPets: [Pet] = []
    var selectedRow = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self,
                                 action: #selector(self.loadPets(_:)),
                                 for: .valueChanged)
        self.tableView.refreshControl = refreshControl
        self.tableView.addSubview(refreshControl)
        
        self.loadPets(refreshControl)
    }
    
    @objc func loadPets(_ refreshControl: UIRefreshControl){
        let userID = UserDefaults.standard.integer(forKey: "userID")
        Alamofire.request("http://vmdev.nexolink.com:90/VetUPCAPI/api/PetsByUser/" + String(userID)).response { response in
            print("Request: \(response.request)")
            print("Response: \(response.response)")
            print("Error: \(response.error)")
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            
            let petsJson = JSON(response.data)
            
            self.myPets.removeAll()
            for (index,subJson):(String, JSON) in petsJson {
                // Do something you want
                let objPet = Pet()
                objPet.Name = subJson["Name"].stringValue
                objPet.Type = subJson["Type"].stringValue
                self.myPets.append(objPet)
            }
            
            self.tableView.reloadData()
            refreshControl.endRefreshing()
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return myPets.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "petCell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = myPets[indexPath.row].Name
        cell.detailTextLabel?.text = myPets[indexPath.row].Type

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedRow = indexPath.row
        print(self.selectedRow)
        self.performSegue(withIdentifier: "viewDetail", sender: self)
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            
            //delete pet from database calling API
            
            self.myPets.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        if(segue.destination is AddPetViewController){
            // Pass the selected object to the new view controller.
            let controller = segue.destination as? AddPetViewController
            if(segue.identifier == "viewDetail"){
                controller?.objPet = myPets[self.selectedRow]
            }
            else{
                controller?.objPet = nil
            }
            print("Prepare Segue")
        }
    }
}
