//
//  FavoritePetsCollectionViewController.swift
//  VetForAll
//
//  Created by Alumnos on 11/12/18.
//  Copyright © 2018 UPC. All rights reserved.
//

import UIKit
import CoreData
import AlamofireImage
import Alamofire

private let reuseIdentifier = "Cell"

class FavoritePetsCollectionViewController: UICollectionViewController {

    var myPets: [Pet] = []
    var selectedRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CDPet")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                let objPet = Pet()
                objPet.ID = data.value(forKey: "id") as! Int
                objPet.Name = data.value(forKey: "name") as! String
                objPet.Type = data.value(forKey: "type") as! String
                objPet.Breed = data.value(forKey: "breed") as! String
                //objPet.DateOfBirth = data.value(forKey: "dateOfBirth") as! Date
                objPet.ImageURL="http://www.slupsk.pl/wp-content/uploads/2018/05/dog-1210559_960_720-696x522.jpg"
                self.myPets.append(objPet)
            }
        } catch {
            print("Failed")
        }
        self.collectionView.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return myPets.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"favpetCell", for: indexPath) as! PetCollectionViewCell
    
        // Configure the cell
        cell.lblTitle.text=""
        cell.lblDetail.text=""
        
        cell.lblTitle?.text=myPets[indexPath.row].Name
        cell.lblDetail?.text=myPets[indexPath.row].Type
        
        Alamofire.request(myPets[indexPath.row].ImageURL).responseImage
            { response in
                debugPrint(response)
                
                print(response.request as Any)
                print(response.response as Any)
                debugPrint(response.result)
                
                if let image = response.result.value {
                    print("image downloaded: \(image)")
                    cell.imagePet.image=image
                }
        }
        
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
