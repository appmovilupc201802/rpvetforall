//
//  VetMapViewController.swift
//  VetForAll
//
//  Created by Profesores on 11/4/18.
//  Copyright © 2018 UPC. All rights reserved.
//

import UIKit
import MapKit

class VetMapViewController: UIViewController {

    var locationX = 0.0
    var locationY = 0.0
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let homeLocation = CLLocation(latitude: locationX, longitude: locationY)
        mapView.showsUserLocation = true
        centerMapOnLocation(location: homeLocation)
        print (locationX)
        print (locationY)
    }
    
    
    let regionRadius: CLLocationDistance = 200
    func centerMapOnLocation(location: CLLocation)
    {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
