//
//  FavoritePetViewController.swift
//  VetForAll
//
//  Created by Alumnos on 11/12/18.
//  Copyright © 2018 UPC. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class FavoritePetViewController: UIViewController {

    
    @IBOutlet var ImagePet: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let objPet=Pet()
        objPet.ImageURL="http://www.slupsk.pl/wp-content/uploads/2018/05/dog-1210559_960_720-696x522.jpg"
        Alamofire.request(objPet.ImageURL).responseImage
            { response in
            debugPrint(response)
            
            print(response.request as Any)
            print(response.response as Any)
            debugPrint(response.result)
            
            if let image = response.result.value {
                print("image downloaded: \(image)")
                self.ImagePet.image=image
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
