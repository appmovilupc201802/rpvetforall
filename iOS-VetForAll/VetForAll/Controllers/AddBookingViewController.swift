//
//  AddBookingViewController.swift
//  VetForAll
//
//  Created by Profesores on 11/3/18.
//  Copyright © 2018 UPC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddBookingViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var petsPickerView: UIPickerView!
    
    @IBOutlet weak var vetsPickerView: UIPickerView!
    
    @IBOutlet weak var servicesPickerView: UIPickerView!
    
    var myPets: [Pet] = []
    var vets: [Vet] = []
    var services: [Service] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        petsPickerView.delegate = self
        petsPickerView.dataSource = self
        
        let userID = UserDefaults.standard.integer(forKey: "userID")
        Alamofire.request("http://vmdev.nexolink.com:90/VetUPCAPI/api/PetsByUser/" + String(userID)).response { response in
            print("Request: \(String(describing: response.request))")
            print("Response: \(String(describing: response.response))")
            print("Error: \(String(describing: response.error))")
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            
            let petsJson = JSON(response.data)
            
            self.myPets.removeAll()
            for (_,subJson):(String, JSON) in petsJson {
                // Do something you want
                let objPet = Pet()
                objPet.ID = subJson["ID"].intValue
                objPet.Name = subJson["Name"].stringValue
                objPet.Type = subJson["Type"].stringValue
                self.myPets.append(objPet)
            }
            self.petsPickerView.reloadAllComponents()
        }
        
        vetsPickerView.delegate = self
        vetsPickerView.dataSource = self
        
        Alamofire.request("http://vmdev.nexolink.com:90/VetUPCAPI/api/vet").response { response in
            print("Request: \(String(describing: response.request))")
            print("Response: \(String(describing:response.response))")
            print("Error: \(String(describing: response.error))")
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            
            let petsJson = JSON(response.data)
            
            self.vets.removeAll()
            for (_,subJson):(String, JSON) in petsJson {
                // Do something you want
                let objVet = Vet()
                objVet.ID = subJson["ID"].intValue
                objVet.Name = subJson["Name"].stringValue
                self.vets.append(objVet)
            }
            self.vetsPickerView.reloadAllComponents()
        }
        
        servicesPickerView.delegate = self
        servicesPickerView.dataSource = self
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case petsPickerView:
            return myPets.count
        case vetsPickerView:
            return vets.count
        case servicesPickerView:
            return services.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case petsPickerView:
            return myPets[row].Name
        case vetsPickerView:
            return vets[row].Name
        case servicesPickerView:
            return services[row].Name
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case vetsPickerView:
            let vetID = vets[row].ID
            Alamofire.request("http://vmdev.nexolink.com:90/VetUPCAPI/api/ServicesByVet/" + String(vetID)).response { response in
                print("Request: \(String(describing: response.request))")
                print("Response: \(String(describing: response.response))")
                print("Error: \(String(describing: response.error))")
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Data: \(utf8Text)")
                }
                
                let petsJson = JSON(response.data)
                
                self.services.removeAll()
                for (_,subJson):(String, JSON) in petsJson {
                    // Do something you want
                    let objService = Service()
                    objService.ID = subJson["ID"].intValue
                    objService.Name = subJson["Name"].stringValue
                    self.services.append(objService)
                }
                self.servicesPickerView.reloadAllComponents()
            }
            break
        default:
            //Nothing at all
            print("Nothing at all")
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
