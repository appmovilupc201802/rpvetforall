//
//  AddBooking2ViewController.swift
//  VetForAll
//
//  Created by Profesores on 11/3/18.
//  Copyright © 2018 UPC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddBooking2ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{

    @IBOutlet weak var txtPet: UITextField!
    @IBOutlet weak var txtVet: UITextField!
    @IBOutlet weak var txtService: UITextField!
    @IBOutlet weak var dpDate: UIDatePicker!
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var bookingPickerView: UIPickerView!
    
    var myPets: [Pet] = []
    var vets: [Vet] = []
    var services: [Service] = []
    
    var selectedPickerView = ""
    
    var selectedPet = 0
    var selectedVet = 0
    var selectedService = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        bookingPickerView.delegate = self
        bookingPickerView.dataSource = self
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch selectedPickerView {
        case "pets":
            return myPets.count
        case "vets":
            return vets.count
        case "services":
            return services.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch selectedPickerView {
        case "pets":
            return myPets[row].Name
        case "vets":
            return vets[row].Name
        case "services":
            return services[row].Name
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        switch selectedPickerView {
        case "pets":
            self.selectedPet = myPets[row].ID
            self.txtPet.text = myPets[row].Name
        case "vets":
            self.selectedVet = vets[row].ID
            self.txtVet.text = vets[row].Name
        case "services":
            self.selectedService = services[row].ID
            self.txtService.text = services[row].Name
        default:
            print("nothing at all")
        }
    }
    
    @IBAction func petTouchDown(_ sender: Any) {
        bookingPickerView.isHidden = false
        selectedPickerView = "pets"
        let userID = UserDefaults.standard.integer(forKey: "userID")
        Alamofire.request("http://vmdev.nexolink.com:90/VetUPCAPI/api/PetsByUser/" + String(userID)).response { response in
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            let petsJson = JSON(response.data!)
            self.myPets.removeAll()
            let defaultValue = Pet()
            defaultValue.ID = 0
            defaultValue.Name = "Select a pet..."
            self.myPets.append(defaultValue)
            for (_,subJson):(String, JSON) in petsJson {
                // Do something you want
                let objPet = Pet()
                objPet.ID = subJson["ID"].intValue
                objPet.Name = subJson["Name"].stringValue
                objPet.Type = subJson["Type"].stringValue
                self.myPets.append(objPet)
            }
            self.bookingPickerView.reloadAllComponents()
        }
    }
    
    @IBAction func vetTouchDown(_ sender: Any) {
        bookingPickerView.isHidden = false
        selectedPickerView = "vets"
        Alamofire.request("http://vmdev.nexolink.com:90/VetUPCAPI/api/vet").response { response in
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            let petsJson = JSON(response.data!)
            self.vets.removeAll()
            let defaultValue = Vet()
            defaultValue.ID = 0
            defaultValue.Name = "Select a vet..."
            self.vets.append(defaultValue)
            for (_,subJson):(String, JSON) in petsJson {
                // Do something you want
                let objVet = Vet()
                objVet.ID = subJson["ID"].intValue
                objVet.Name = subJson["Name"].stringValue
                objVet.LocationX = subJson["LocationX"].doubleValue
                objVet.LocationY = subJson["LocationY"].doubleValue
                self.vets.append(objVet)
            }
            self.bookingPickerView.reloadAllComponents()
        }
    }
    
    @IBAction func serviceTouchDown(_ sender: Any) {
        bookingPickerView.isHidden = false
        selectedPickerView = "services"
        Alamofire.request("http://vmdev.nexolink.com:90/VetUPCAPI/api/ServicesByVet/" + String(self.selectedVet)).response { response in
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            let petsJson = JSON(response.data)
            self.services.removeAll()
            let defaultValue = Service()
            defaultValue.ID = 0
            defaultValue.Name = "Select a service..."
            self.services.append(defaultValue)
            for (_,subJson):(String, JSON) in petsJson {
                // Do something you want
                let objService = Service()
                objService.ID = subJson["ID"].intValue
                objService.Name = subJson["Name"].stringValue
                self.services.append(objService)
            }
            self.bookingPickerView.reloadAllComponents()
        }
    }
    
    @IBAction func dpDateTouchDown(_ sender: Any) {
        bookingPickerView.isHidden = true
    }
    
    
    @IBAction func txtTimeTouchDown(_ sender: Any) {
        bookingPickerView.isHidden = true
    }
    
    @IBAction func btnSavePressed(_ sender: Any) {
        bookingPickerView.isHidden = true
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let petParameters: Parameters = [
            "PetID": selectedPet,
            "ServiceID": selectedService,
            "Date": formatter.string(from: dpDate.date),
            "Time": self.txtTime.text!
            ]
        
        Alamofire.request("http://vmdev.nexolink.com:90/VetUPCAPI/api/Booking",
                          method: .post,
                          parameters: petParameters,
                          encoding: JSONEncoding.default)
            .response { response in
                print("Request: \(response.request)")
                print("Response: \(response.response)")
                print("Error: \(response.error)")
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Data: \(utf8Text)")
                }
                
                var message = ""
                var title = ""
                if(response.error == nil){
                    title = "Success"
                    message = "Booking saved!"
                }
                else{
                    title = "Error"
                    message = "An error has occurred! Please try again!"
                }
                
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                    NSLog("The \"OK\" alert occured.")
                    
                    if(title == "Success"){
                        self.navigationController?.popViewController(animated: true)
                    }
                }))
                self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if(segue.destination is VetMapViewController){
            // Pass the selected object to the new view controller.
            let controller = segue.destination as? VetMapViewController
            if(segue.identifier == "viewMap"){
                controller?.locationX = self.vets[self.selectedVet].LocationX
                controller?.locationY = self.vets[self.selectedVet].LocationY
            }
            else{

            }
        }
    }
 

}
