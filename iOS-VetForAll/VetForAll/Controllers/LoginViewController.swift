//
//  LoginViewController.swift
//  VetForAll
//
//  Created by Alumnos on 10/13/18.
//  Copyright © 2018 UPC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController {

    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let username = UserDefaults.standard.string(forKey: "username")
        self.txtUserName.text = username
    }
    
    @IBAction func btnLoginPressed(_ sender: Any) {
        let username = self.txtUserName.text
        let password = self.txtPassword.text
        
        if ( username != "" && password != ""){
            
            Alamofire.request("http://vmdev.nexolink.com:90/VetUPCAPI/api/User?email=" + username! + "&password=" + password!).responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                if let json = response.result.value {
                    print("JSON: \(json)") // serialized json response
                    let userJson = JSON(json)
                    let id = userJson["ID"].intValue
                    let username = userJson["Username"].stringValue
                    
                    //Stored in phone
                    UserDefaults.standard.set(id, forKey: "userID")
                    UserDefaults.standard.set(username, forKey: "username")
                    
                    self.performSegue(withIdentifier: "login", sender: self)
                }
                else{
                    let alert = UIAlertController(title: "Fail!", message: "Invalid user name or password.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                        NSLog("The \"OK\" alert occured.")
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Data: \(utf8Text)") // original server data as UTF8 string
                }
            }
        }
        else{
            let alert = UIAlertController(title: "Fail!", message: "Invalid user name or password.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
