//
//  PetCollectionViewCell.swift
//  VetForAll
//
//  Created by Alumnos on 11/12/18.
//  Copyright © 2018 UPC. All rights reserved.
//

import UIKit

class PetCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDetail: UILabel!
    @IBOutlet var imagePet: UIImageView!
    
}
