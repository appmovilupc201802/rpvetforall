//
//  AddPetViewController.swift
//  VetForAll
//
//  Created by Alumnos on 10/14/18.
//  Copyright © 2018 UPC. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class AddPetViewController: UIViewController {

    //Outlets
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtBreed: UITextField!
    @IBOutlet weak var dpDateOfBirth: UIDatePicker!
    
    //Parameters
    var objPet : Pet? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if(self.objPet != nil){
            self.txtName.text = self.objPet?.Name
            self.txtType.text = self.objPet?.Type
            print("Load data")
        }
        else{
            self.txtName.text = ""
            self.txtType.text = ""
            print("Clear data")
        }
    }
    
    @IBAction func btnSavePressed(_ sender: Any) {
        
        let objPet = Pet()
        objPet.ID = 0
        objPet.UserID = UserDefaults.standard.integer(forKey: "userID")
        objPet.Name = txtName.text!
        objPet.Type = txtType.text!
        objPet.Breed = txtBreed.text!
        objPet.DateOfBirth = dpDateOfBirth.date
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let petParameters: Parameters = [
            "UserID": objPet.UserID,
            "Name": objPet.Name,
            "Type": objPet.Type,
            "Breed": objPet.Breed,
            "DateOfBirth": formatter.string(from: objPet.DateOfBirth),
        ]
        
        Alamofire.request("http://vmdev.nexolink.com:90/VetUPCAPI/api/Pet",
                          method: .post,
                          parameters: petParameters,
                          encoding: JSONEncoding.default)
            .response { response in
            print("Request: \(response.request)")
            print("Response: \(response.response)")
            print("Error: \(response.error)")
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            
            var message = ""
            var title = ""
            if(response.error == nil){
                title = "Success"
                message = "Pet saved!"
            }
            else{
                title = "Error"
                message = "An error has occurred! Please try again!"
            }
                
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
                
                if(title == "Success"){
                    self.navigationController?.popViewController(animated: true)
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func favoritePressed(_ sender: Any) {
        if(self.objPet != nil){
            //Save to core data
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: "CDPet", in: context)
            let newPet = NSManagedObject(entity: entity!, insertInto: context)
            newPet.setValue(self.objPet!.ID, forKey: "id")
            newPet.setValue(self.objPet!.Name, forKey: "name")
            newPet.setValue(self.objPet!.Type, forKey: "type")
            newPet.setValue(self.objPet!.Breed, forKey: "breed")
            newPet.setValue(self.objPet!.DateOfBirth, forKey: "dateOfBirth")
            
            var message = ""
            var title = ""
            
            do {
                try
                context.save()
                title = "Success!"
                message = "Pet saved in favorites!"
            } catch {
                title = "Error!"
                message = "Error saving this pet in favorites"
                print("Failed saving")
            }
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
