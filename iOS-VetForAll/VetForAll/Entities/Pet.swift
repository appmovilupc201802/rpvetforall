//
//  Pet.swift
//  VetForAll
//
//  Created by Alumnos on 10/14/18.
//  Copyright © 2018 UPC. All rights reserved.
//

import UIKit

class Pet: NSObject {
    var ID = 0
    var UserID = 0
    var Name: String = ""
    var `Type`: String = ""
    var Breed: String = ""
    var DateOfBirth: Date = Date()
    var ImageURL:String=""
}
