//
//  Service.swift
//  VetForAll
//
//  Created by Profesores on 11/3/18.
//  Copyright © 2018 UPC. All rights reserved.
//

import UIKit

class Service: NSObject {
    var ID = 0
    var ServiceTypeID = 0
    var VetID = 0
    var Name = ""
    var Description = ""
    var Price = 0.0
}
