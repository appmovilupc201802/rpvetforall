//
//  Vet.swift
//  VetForAll
//
//  Created by Profesores on 11/3/18.
//  Copyright © 2018 UPC. All rights reserved.
//

import UIKit

class Vet: NSObject {
    var ID = 0
    var Name = ""
    var Address = ""
    var LocationX = 0.0
    var LocationY = 0.0
}
