//
//  Booking.swift
//  VetForAll
//
//  Created by Profesores on 10/11/18.
//  Copyright © 2018 UPC. All rights reserved.
//

import UIKit

class Booking: NSObject {
    var ID = 0
    var BookingDate : Date = Date()
    var Time = ""
    var VetName = ""
    var ServiceName = ""
    var PetName = ""
}
