package upc.edu.pe.vetforall.Entities;

import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by alumnos on 9/15/18.
 */

public class Booking extends SugarRecord<Booking> {

    public Booking(){

    }

    public long ID;
    public long ServiceID;
    public long PetID;
    public Date Date;
    public String Time;
    public String VetName;
    public String ServiceName;
    public String PetName;
}
