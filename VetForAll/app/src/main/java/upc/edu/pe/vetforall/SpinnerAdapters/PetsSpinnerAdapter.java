package upc.edu.pe.vetforall.SpinnerAdapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import upc.edu.pe.vetforall.Entities.Pet;

/**
 * Created by alumnos on 9/10/18.
 */

public class PetsSpinnerAdapter extends ArrayAdapter<Pet> {
    private Context context;
    List<Pet> data = null;

    //Constructor
    public PetsSpinnerAdapter(Context context,
                              int resource,
                              List<Pet> data2)
    {
        super(context, resource, data2);
        this.context = context;
        this.data = data2;
    }

    @Nullable
    @Override
    public Pet getItem(int position) {
        return data.get(position);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).ID;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.
                getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(data.get(position).Name);

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    @Override
    public View getDropDownView(int position,
                                @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        TextView label = (TextView) super.
                getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLUE);
        label.setText(data.get(position).Name);
        label.setTextSize(40);
        return label;
    }
}
