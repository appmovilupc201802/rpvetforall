package upc.edu.pe.vetforall.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Spinner;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import upc.edu.pe.vetforall.Entities.Booking;
import upc.edu.pe.vetforall.Entities.Pet;
import upc.edu.pe.vetforall.Entities.Service;
import upc.edu.pe.vetforall.Entities.Vet;
import upc.edu.pe.vetforall.R;
import upc.edu.pe.vetforall.SpinnerAdapters.PetsSpinnerAdapter;
import upc.edu.pe.vetforall.SpinnerAdapters.ServicesSpinnerAdapter;
import upc.edu.pe.vetforall.SpinnerAdapters.VetsSpinnerAdapter;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BookFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BookFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public BookFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BookFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BookFragment newInstance(String param1, String param2) {
        BookFragment fragment = new BookFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View myView = inflater
                .inflate(R.layout.fragment_book,
                        container,
                        false);

        //Read email from preferences
        SharedPreferences prefs = getActivity().
                getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        int storedUserID = prefs.getInt("UserID", 0);

        AndroidNetworking.
                initialize(getActivity().getApplicationContext());

        //Load user's pets
        final Spinner spnrPets = myView.findViewById(R.id.spinnerPets);
        AndroidNetworking
                .get("http://vmdev1.nexolink.com:90/VetUPCAPI/api/PetsByUser/{userId}")
                .addPathParameter("userId", Integer.toString(storedUserID))
                .setTag("pets")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // do anything with response
                        try {
                            //Create list
                            ArrayList<Pet> myPets = new ArrayList<Pet>();
                            //Enter default value
                            Pet defaultValue = new Pet();
                            defaultValue.ID = 0;
                            defaultValue.Name = "Select a Pet...";
                            myPets.add(defaultValue);

                            //Read response
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                Pet objPet = new Pet();
                                objPet.ID = jsonObject.getInt("ID");
                                objPet.Name = jsonObject.getString("Name");
                                myPets.add(objPet);
                            }
                            //Create adapter
                            PetsSpinnerAdapter petsSpinnerAdapter =
                                    new PetsSpinnerAdapter(myView.getContext(),
                                            android.R.layout.simple_spinner_item,
                                            myPets);

                            //Configure spinner
                            spnrPets.setAdapter(petsSpinnerAdapter);
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });

        //Load Vets
        final Spinner spnrVets = myView.findViewById(R.id.spnrVets);
        AndroidNetworking
                .get("http://vmdev1.nexolink.com:90/VetUPCAPI/api/Vet")
                .setTag("vets")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            //Create list
                            ArrayList<Vet> vets = new ArrayList<Vet>();
                            //Set default value
                            Vet defaultValue = new Vet();
                            defaultValue.ID = 0;
                            defaultValue.Name = "Select a Vet...";
                            vets.add(defaultValue);

                            //Read response
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                Vet objVet = new Vet();
                                objVet.ID = jsonObject.getInt("ID");
                                objVet.Name = jsonObject.getString("Name");
                                objVet.Address = jsonObject.getString("Address");
                                objVet.LocationX = jsonObject.getDouble("LocationX");
                                objVet.LocationY = jsonObject.getDouble("LocationY");
                                vets.add(objVet);
                            }
                            //Create adapter
                            VetsSpinnerAdapter vetsSpinnerAdapter =
                                    new VetsSpinnerAdapter(myView.getContext(),
                                            android.R.layout.simple_spinner_item,
                                            vets);

                            //Configure spinner
                            spnrVets.setAdapter(vetsSpinnerAdapter);
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        // handle error
                    }
                });

        //Implement on selected item
        final Spinner spnrServices = myView.findViewById(R.id.spnrServices);
        spnrVets.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long vetID) {
                AndroidNetworking
                        .get("http://vmdev1.nexolink.com:90/VetUPCAPI/api/ServicesByVet/{vetID}")
                        .addPathParameter("vetID", Long.toString(vetID))
                        .setTag("services")
                        .setPriority(Priority.LOW)
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                // do anything with response
                                try {
                                    //Create list
                                    ArrayList<Service> services = new ArrayList<Service>();
                                    //Enter default value
                                    Service defaultValue = new Service();
                                    defaultValue.ID = 0;
                                    defaultValue.Name = "Select a Service...";
                                    services.add(defaultValue);

                                    //Read response
                                    for (int i = 0; i < response.length(); i++) {
                                        JSONObject jsonObject = response.getJSONObject(i);
                                        Service objService = new Service();
                                        objService.ID = jsonObject.getInt("ID");
                                        objService.Name = jsonObject.getString("Name");
                                        objService.Description = jsonObject.getString("Description");
                                        objService.Price = jsonObject.getDouble("Price");
                                        services.add(objService);
                                    }
                                    //Create adapter
                                    ServicesSpinnerAdapter servicesSpinnerAdapter =
                                            new ServicesSpinnerAdapter(myView.getContext(),
                                                    android.R.layout.simple_spinner_item,
                                                    services);

                                    //Configure spinner
                                    spnrServices.setAdapter(servicesSpinnerAdapter);
                                }
                                catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        final CalendarView cvBookingDate = myView.findViewById(R.id.cvDate);

        //On button click
        Button btnBook = myView.findViewById(R.id.btnBook);
        final EditText txtHour = myView.findViewById(R.id.txtHour);
        btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Booking objBooking = new Booking();
                objBooking.Date = new Date(cvBookingDate.getDate());
                objBooking.PetID = spnrPets.getSelectedItemId();
                objBooking.ServiceID = spnrServices.getSelectedItemId();
                objBooking.Time = txtHour.getText().toString();

                AndroidNetworking.post("http://vmdev1.nexolink.com:90/VetUPCAPI/api/Booking")
                        .addBodyParameter(objBooking) // posting java object
                        .setTag("booking")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // do anything with response
                                Log.println(Log.ASSERT, "Success", "It worked!!!");
                                mListener.onBookingCreated();
                            }

                            @Override
                            public void onError(ANError error) {
                                // handle error
                                Log.println(Log.ERROR, "Error",
                                        "Some error!!" + error.getErrorBody()
                                                + " - " + error.getErrorDetail()
                                                + " - " + error.getResponse());
                            }
                        });
            }
        });

        return myView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        void onBookingCreated();
    }
}
