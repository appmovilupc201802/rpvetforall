package upc.edu.pe.vetforall.Activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import upc.edu.pe.vetforall.Entities.Booking;
import upc.edu.pe.vetforall.Fragments.AddPetFragment;
import upc.edu.pe.vetforall.Fragments.BookFragment;
import upc.edu.pe.vetforall.Fragments.HomeFragment;
import upc.edu.pe.vetforall.Fragments.MyBookingsFragment;
import upc.edu.pe.vetforall.Fragments.PetsFragment;
import upc.edu.pe.vetforall.Fragments.ProfileFragment;
import upc.edu.pe.vetforall.Fragments.dummy.DummyContent;
import upc.edu.pe.vetforall.R;

public class MainActivity extends AppCompatActivity
implements HomeFragment.OnFragmentInteractionListener,
        ProfileFragment.OnFragmentInteractionListener,
        PetsFragment.OnPetsFragmentInteractionListener,
        BookFragment.OnFragmentInteractionListener,
        MyBookingsFragment.OnListFragmentInteractionListener,
        AddPetFragment.OnFragmentInteractionListener {

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    //mTextMessage.setText(R.string.title_home);
                    HomeFragment homeFragment = new HomeFragment();
                    replaceFragment(homeFragment);
                    return true;
                case R.id.navigation_my_bookings:
                    //mTextMessage.setText(R.string.nav_title_my_bookings);
                    MyBookingsFragment myBookingsFragment = new MyBookingsFragment();
                    replaceFragment(myBookingsFragment);
                    return true;
                case R.id.navigation_book:
                    //mTextMessage.setText(R.string.nav_title_book);
                    BookFragment bookFragment = new BookFragment();
                    replaceFragment(bookFragment);
                    return true;
                case R.id.navigation_pets:
                    //mTextMessage.setTextSize(R.string.nav_title_pets);
                    PetsFragment petsFragment = new PetsFragment();
                    replaceFragment(petsFragment);
                    return true;
                case R.id.navigation_profile:
                    //mTextMessage.setTextSize(R.string.nav_title_profile);
                    ProfileFragment profileFragment = new ProfileFragment();
                    replaceFragment(profileFragment);
                    return true;
            }
            return false;
        }
    };

    private void replaceFragment(Fragment myFragment){
        FragmentManager fragmentManager =
                getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_layout, myFragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        HomeFragment homeFragment = new HomeFragment();
        replaceFragment(homeFragment);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onAddPetPressed() {
        AddPetFragment addPetFragment = new AddPetFragment();
        replaceFragment(addPetFragment);
    }

    @Override
    public void onListFragmentInteraction(Booking item) {
        Log.d("test", item.PetName);
        item.save();
    }

    @Override
    public void onBookingCreated() {
        BottomNavigationView bnvVetForAll = findViewById(R.id.navigation);
        bnvVetForAll.setSelectedItemId(R.id.navigation_my_bookings);
    }
}
