package upc.edu.pe.vetforall.SpinnerAdapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import upc.edu.pe.vetforall.Entities.Pet;
import upc.edu.pe.vetforall.Entities.Vet;

/**
 * Created by alumnos on 9/15/18.
 */

public class VetsSpinnerAdapter extends ArrayAdapter<Vet> {
    private Context context;
    List<Vet> data = null;

    //Constructor
    public VetsSpinnerAdapter(Context context,
                              int resource,
                              List<Vet> data2)
    {
        super(context, resource, data2);
        this.context = context;
        this.data = data2;
    }

    @Nullable
    @Override
    public Vet getItem(int position) {
        return data.get(position);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).ID;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.
                getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(data.get(position).Name);

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.
                getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLUE);
        label.setText(data.get(position).Name);
        label.setTextSize(40);
        return label;
    }
}
