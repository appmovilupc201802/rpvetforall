package upc.edu.pe.vetforall.Fragments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import upc.edu.pe.vetforall.Entities.Booking;
import upc.edu.pe.vetforall.Fragments.MyBookingsFragment.OnListFragmentInteractionListener;
import upc.edu.pe.vetforall.Fragments.dummy.DummyContent.DummyItem;
import upc.edu.pe.vetforall.R;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyBookingsRecyclerViewAdapter extends RecyclerView.Adapter<MyBookingsRecyclerViewAdapter.ViewHolder> {

    private final List<Booking> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyBookingsRecyclerViewAdapter(List<Booking> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_booking, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(Long.toString(mValues.get(position).ID));
        holder.mPetNameView.setText(mValues.get(position).PetName);
        holder.mVetNameView.setText(mValues.get(position).VetName);
        holder.mServiceNameView.setText(mValues.get(position).ServiceName);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mPetNameView;
        public final TextView mVetNameView;
        public final TextView mServiceNameView;
        public Booking mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mPetNameView = (TextView) view.findViewById(R.id.pet_name);
            mVetNameView = (TextView) view.findViewById(R.id.vet_name);
            mServiceNameView = (TextView) view.findViewById(R.id.service_name);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mPetNameView.getText() + "'";
        }
    }
}
