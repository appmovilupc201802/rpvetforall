package upc.edu.pe.vetforall.Entities;

import java.util.Date;

/**
 * Created by alumnos on 9/3/18.
 */

public class Pet {
    public int ID;
    public String Name;
    public String Type;
    public String Breed;
    public Date DateOfBirth;
    public int UserID;
}
