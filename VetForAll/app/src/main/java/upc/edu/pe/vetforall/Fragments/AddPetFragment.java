package upc.edu.pe.vetforall.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;

import upc.edu.pe.vetforall.Entities.Pet;
import upc.edu.pe.vetforall.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddPetFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddPetFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddPetFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AddPetFragment() {
        // Required empty public constructor
    }

     /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddPetFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddPetFragment newInstance(String param1, String param2) {
        AddPetFragment fragment = new AddPetFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        AndroidNetworking.initialize(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View myView = inflater.inflate(R.layout.fragment_add_pet, container, false);

        Button btnAddPet = myView.findViewById(R.id.btnSave);

        btnAddPet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText txtName = myView.findViewById(R.id.txtName);
                EditText txtType = myView.findViewById(R.id.txtType);
                EditText txtBreed = myView.findViewById(R.id.txtBreed);
                CalendarView cvDateOfBirth = myView.findViewById(R.id.cvDateOfBirth);

                Pet objPet = new Pet();
                objPet.Name = txtName.getText().toString();
                objPet.Type = txtType.getText().toString();
                objPet.Breed = txtBreed.getText().toString();
                objPet.DateOfBirth = new Date(cvDateOfBirth.getDate());

                //Read user id from preferences
                SharedPreferences prefs = getActivity().
                        getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
                int storedUserID = prefs.getInt("UserID", 0);
                objPet.UserID = storedUserID;

                AndroidNetworking.post("http://vmdev1.nexolink.com:90/VetUPCAPI/api/Pet")
                        .addBodyParameter(objPet) // posting java object
                        .setTag("pet")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // do anything with response
                                Log.println(Log.ASSERT, "Success", "It worked!!!");
                            }

                            @Override
                            public void onError(ANError error) {
                                // handle error
                                Log.println(Log.ERROR, "Error",
                                        "Some error!!" + error.getErrorBody()
                                                + " - " + error.getErrorDetail()
                                                + " - " + error.getResponse());
                            }
                        });
            }
        });

        return myView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
